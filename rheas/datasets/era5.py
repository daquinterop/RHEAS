import cdsapi
from datetime import timedelta
from . import datasets
import logging
import tempfile
import os
# import shutil
import zipfile
from .decorators import netcdf

TMPDIR = tempfile.gettempdir()
VARIABLES = {
    '2m_temperature': ['24_hour_maximum', '24_hour_minimum'],
    '10m_wind_speed': '24_hour_mean'
}

table = ["tmax.era5", "tmin.era5", "wind.era5"]

def dates(dbname):
    dts = datasets.dates(dbname, "wind.era5")
    return dts

@netcdf
def fetch_tmax(dbname, dt, bbox):
    filepath = os.path.join(TMPDIR, f'2m_temperature_{dt.strftime("%Y%m%d")}')
    with zipfile.ZipFile(f'{filepath}.zip', 'r') as zip_ref:
        for file in zip_ref.filelist:
            filename = file.filename
            if 'Max' in filename:
                break
        zip_ref.extract(filename, path=TMPDIR)
    filepath = os.path.join(TMPDIR, filename)
    varname = 'Temperature_Air_2m_Max_24h'
    return filepath, varname, bbox, dt

@netcdf
def fetch_tmin(dbname, dt, bbox):
    filepath = os.path.join(TMPDIR, f'2m_temperature_{dt.strftime("%Y%m%d")}')
    with zipfile.ZipFile(f'{filepath}.zip', 'r') as zip_ref:
        for file in zip_ref.filelist:
            filename = file.filename
            if 'Min' in filename:
                break
        zip_ref.extract(filename, path=TMPDIR)
    filepath = os.path.join(TMPDIR, filename)
    varname = 'Temperature_Air_2m_Min_24h'
    return filepath, varname, bbox, dt

@netcdf
def fetch_wnd(dbname, dt, bbox):
    filepath = os.path.join(TMPDIR, f'10m_wind_speed_{dt.strftime("%Y%m%d")}')
    with zipfile.ZipFile(f'{filepath}.zip', 'r') as zip_ref:
        for file in zip_ref.filelist:
            filename = file.filename
            if 'Wind' in filename:
                break
        zip_ref.extract(filename, path=TMPDIR)
    filepath = os.path.join(TMPDIR, filename)
    varname = 'Wind_Speed_10m_Mean'
    return filepath, varname, bbox, dt

def download(dbname, dts, bbox=None):
    log = logging.getLogger(__name__)
    res = .1
    try:
        c = cdsapi.Client()
    except Exception:
        log.error("Missing/incomplete configuration file: ~/.cdsapirc. Follow the instructions at https://cds.climate.copernicus.eu/api-how-to")
        raise Exception
    time_i, end_time = dts
    bbox_cds = [bbox[3], bbox[0], bbox[1], bbox[2]]
    while time_i <= end_time:
        for variable, statistic in VARIABLES.items():
            year, month, day = time_i.strftime('%Y %m %d').split()
            filename = os.path.join(TMPDIR, f'{variable}_{year}{month}{day}.zip')
            c.retrieve(
                'sis-agrometeorological-indicators',
                {
                    'format': 'zip',
                    'variable': variable,
                    'year': year,
                    'month': month,
                    'day': day,
                    'area': bbox_cds,
                    'statistic': statistic,
                }, filename
            )

            if 'temperature' in variable:
                tmax, lat, lon, _ = fetch_tmax(dbname, time_i, bbox)
                tmax = tmax - 273.15
                datasets.ingest(dbname, "tmax.era5", tmax[0, :, :], lat, lon, res, time_i)
                tmin, _, _, _ = fetch_tmin(dbname, time_i, bbox)
                tmin = tmin - 273.15
                datasets.ingest(dbname, "tmin.era5", tmin[0, :, :], lat, lon, res, time_i)
            elif 'wind' in variable:
                wnd, _, _, _ = fetch_wnd(dbname, time_i, bbox)
                datasets.ingest(dbname, "wind.era5", wnd[0, :, :], lat, lon, res, time_i)
            else:
                pass

            with zipfile.ZipFile(filename, 'r') as zip_ref:
                for file in zip_ref.filelist:
                    os.remove(os.path.join(TMPDIR, file.filename))
            os.remove(filename)

        time_i += timedelta(days=1)
    return